const videoItem = {

  video: {
    id: '11',
    type: 'stream',
    timeLength: 11211,
    title: 'partA',
    url: 'http://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8',
  },

  videoConfig: {
    preloadTime: undefined,
    canDownload: false,
    autoPlay: true,
    width: undefined,
    height: undefined,
    nativeControl: undefined
  },
  story: {
    title: 'partA',
    description: '',
    tag: [''],
    note: '',
    account: 'all',
    interactiveStyle: {
      position: 'absolute',
      top: 0,
      left: 0,
      zIndex: 1,
    },
    buttonStyle: {
      // backgroundColor: '#66ccff',
      width: 144,
      height: 64,
      borderWidth: 1,
      borderColor: '#fff',
      backgroundColor: 'rgba(255,255,255,0.3)',
      margin: 64
    },
    textStyle: {
      color: '#fff'
    },

    control: {
      jump: false,
      startTime: undefined,
      endTime: undefined,
    }

  },
  nextStories: [
    {
      story: {
        title: 'partB',
        description: '',
        tag: [''],
        note: '',
        account: 'all',
        buttonTitle: 'partB',
        buttonIcon: undefined,
        control: {
          jump: false,
          startTime: undefined,
          endTime: undefined,
        }
      },
      videoConfig: {
        preloadTime: undefined,
        canDownload: false,
        autoPlay: true,
        width: undefined,
        height: undefined
      }
      ,
      video: {
        id: '11',
        type: 'stream',
        timeLength: 11211,
        title: 'partB',
        url: 'https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8'
      }
      ,
      style: {
        row: 'center',
        column: 'footer',
        padding: '20%',
      }
    },
    {
      story: {
        title: 'partC',
        description: '',
        tag: [''],
        note: '',
        account: 'all',
        buttonTitle: 'partC',
        buttonIcon: undefined,
        buttonStyle: {},
        control: {
          jump: false,
          startTime: undefined,
          endTime: undefined,
        }
      }
      ,
      videoConfig: {
        preloadTime: undefined,
        canDownload: false,
        autoPlay: true
      }
      ,
      video: {
        id: '12',
        type: 'stream',
        timeLength: 11211,
        title: 'partC',
        url: 'https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8'
      }
      ,
      style: {
        row: 'center',
        column: 'footer',
        padding: '20%'
      }
    },

  ],
}
export default videoItem

