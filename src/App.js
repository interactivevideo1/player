import logo from './logo.svg'
import './App.css'
import InteractivePlayer from './InteractiveVideoPlayer'
import videoItem from './defautlInfo'

const handleChangeVideo = (node) => {
  console.log(node)
}

const loadingCallback = () => {

}

const App = () => {
  return (
    <div className="App">
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
      <InteractivePlayer videoItem={videoItem} loadingCallback={loadingCallback} onChange={handleChangeVideo}
                         style={{width: 1000}}/>
    </div>
  )
}

export default App
