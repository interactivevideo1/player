import React, { useRef, useEffect, useState } from 'react'
import './App.css'
import Hls from 'hls.js'

const InteractivePlayer = ({videoItem = {}, onChange = () => {}, loadingCallback = () => {}}) => {

  const {video, story, nextStories, videoConfig} = videoItem

  const [loading, setLoading] = useState(false)
  const [showInteractive, setShowInteractive] = useState(false)

  useEffect(() => {
    if (video && video.url) {
      fetchVideo()
    }
  }, [])

  useEffect(() => {
    listener()
  })

  const nextHls = nextStories.map(() => {
    return new Hls()
  })

  const player = useRef(null)

  const fetchVideo = async () => {
    const url = videoItem.video.url
    if (video.type === 'file' && player.current.canPlayType('application/vnd.apple.mpegurl')) {
      player.current.src = url
    }
    if (video.type === 'stream' && Hls.isSupported()) {
      const hls = new Hls()
      hls.loadSource(url)
      hls.attachMedia(player.current)
    } else {
      // showError
    }
  }

  const preloadNextVideo = () => {
    // eslint-disable-next-line array-callback-return
    nextHls.filter((item, index) => {
      const url = nextStories[index].video.url
      item.loadSource(url)
    })
  }

  const handleChangeVideo = (index) => {
    setShowInteractive(false)
    onChange(nextStories[index])
    nextHls[index].attachMedia(player.current)
  }

  const listener = () => {
    const refreshIntervalId = setInterval(() => {
      const preloadTime = videoConfig.preloadTime || 5
      const duration = parseInt(player.current.duration)
      const currentTime = parseInt(player.current.currentTime)
      if (duration - currentTime <= preloadTime) {
        clearInterval(refreshIntervalId)
        setShowInteractive(true)
        preloadNextVideo()
      }
    }, 1000)

  }

  return (
    <div style={{position: 'relative'}}>
      <video
        controls={videoConfig.nativeControl || false}
        ref={player}
        muted
        autoPlay={videoConfig.autoPlay || true}
        style={Object.assign({}, videoConfig.style, {zIndex: -1, position: 'relative'})}
        width={videoConfig.width || '100%'}
        height={videoConfig.height || '100%'}
      />
      {showInteractive &&
       <div className='interacitve-div' style={story.interactiveStyle}>
         {
           nextStories.map((item, index) =>
             <button key={index} style={story.buttonStyle} onClick={() => handleChangeVideo(index)}>
               <span style={story.textStyle}>{item.story.buttonTitle || item.story.title}</span>
             </button>
           )
         }
       </div>
      }
    </div>
  )
}

export default InteractivePlayer
